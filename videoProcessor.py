#!/usr/bin/python

import cv2
import numpy as np


def singleton(cls, *args, **kw):
    instances = {}
    def _singleton():
       if cls not in instances:
            instances[cls] = cls(*args, **kw)
       return instances[cls]
    return _singleton

@singleton
class videoProcessor():
     def __init__(self, ):
         self.fileconfigpath='/mongkoys/pics/for/test'
         self.cam = cv2.VideoCapture(0)
         self.count = int()
         self.pic_num = 448
     def __del__(self):
         self.cam.release()
         cv2.destroyAllWindows()

     def getCapture(self,*args):
         ret, frame = self.cam.read()
          # Our operations on the frame come here
         captureImage = cv2.cvtColor(frame,1)
         ret, jpeg = cv2.imencode('.jpg', captureImage)
         return jpeg.tobytes()

         
     def getStream(self, *args):
         ''' test stream using multiple images'''
         self.count +=1             
         self.pic_num = self.pic_num + 1
         if self.pic_num > 477:
             self.pic_num = 448
         filename = self.fileconfigpath + str(self.pic_num) + '.JPG'
         print filename
         return open(filename,'rb').read()
