#!/usr/bin/env python

import stat, os
import tempfile
from flask import Flask, render_template, Response
from videoProcessor import  videoProcessor


app = Flask(__name__,template_folder='template')

@app.route('/')
def index():
    """Video streaming home page."""
    return render_template('index.html')

def genJpeg(videocam):
    while True:
        img = videocam.getCapture()
        yield (b'--frame\r\n'
        b'Content-Type: image/png\r\n\r\n' + img + b'\r\n')
        
@app.route('/feed')
def feed():
    return Response(genJpeg(videoProcessor()),
                    mimetype='multipart/x-mixed-replace; boundary=frame')


if __name__ == '__main__':

    app.run(host='0.0.0.0', debug=True, threaded=True)
    
